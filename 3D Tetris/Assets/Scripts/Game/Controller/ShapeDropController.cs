﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using UnityEngine;

using Zenject;

using Tetris3D.Settings;
using Tetris3D.Model;
using Tetris3D.Events;
using static Tetris3D.Utilities.NullChecker;

namespace Tetris3D.Controller
{
  public class ShapeDropController : MonoBehaviour
  {
    [Inject] private MapData _map = default;
    [Inject] private ShapeQueue _shapeQueue = default;
    [Inject] private GameSettings _gameSettings = default;
    [Inject] private OnShapeCollidedEvent _onShapeCollided = default;

    private float _dropCooldown
      => _gameSettings.FetchShapeDropCooldown(_map.PlacedShapesCount);

    private IShapeData _currentShape => _shapeQueue.CurrentShape;
    private float _timer;

    private void Awake()
    {
      resetTimer();
    }

    private void Update()
    {
      shapeBehaviour(Time.deltaTime);
      calculatePreviewOffset();
    }

    public void ForceShapeDrop()
    {
      if (NotNull(_currentShape))
        shapeBehaviour(Mathf.Infinity);
    }

    private void shapeBehaviour(float deltaTime)
    {
      if (IsNull(_currentShape))
      {
        resetTimer();
        return;
      }

      if (_timer <= 0f)
      {
        resetTimer();
        dropShape();
        return;
      }

      _timer -= deltaTime;
    }

    private void resetTimer()
      => _timer = _dropCooldown;

    private void dropShape()
    {
      if (moveShapeDown() == false)
      {
        _onShapeCollided.Invoke();
        return;
      }
    }

    private void calculatePreviewOffset()
    {
      if (IsNull(_currentShape))
        return;

      int y = _gameSettings.MapSize.y;

      while (y >= 0)
      {
        _currentShape.PreviewOffset = new Vector3Int(0, y, 0);

        if (_map.AnySpaceOccupied(_currentShape.PreviewGridPositions) == false)
          break;

        y--;
      }
    }

    private bool moveShapeDown()
    {
      if (currentShapeCanDrop() == false)
        return false;

      _currentShape.MovePivot(_currentShape.PivotPosition + Vector3Int.down);
      return true;
    }

    private bool currentShapeCanDrop()
    {
      Vector3Int down = Vector3Int.down;

      foreach (Vector3Int gridPosition in _currentShape.GridPositions)
      {
        Vector3Int targetPos = gridPosition + down;

        if (targetPos.y < 0)
          return false;

        if (_map.SpaceOccupied(targetPos))
          return false;
      }

      return true;
    }
  }
}