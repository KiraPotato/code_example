﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using UnityEngine;

using Zenject;

using Tetris3D.Model;
using static Tetris3D.Utilities.NullChecker;

namespace Tetris3D.Controller
{
  public class ShapeController : MonoBehaviour
  {
    [Inject] private MapData _map = default;
    [Inject] private ShapeQueue _queue = default;
    [Inject] private CameraDirection _cameraDirections = default;

    IShapeData _currentShape => _queue.CurrentShape;

    public void MoveShape(int xDirection, int zDirection)
    {
      if (IsNull(_currentShape) || (xDirection == 0 && zDirection == 0))
        return;

      Vector3Int dir = new Vector3Int(xDirection, 0, zDirection);

      if (canMoveInDirection(dir) == false)
        return;

      Vector3Int newPivotPos = _currentShape.PivotPosition + dir;
      _currentShape.MovePivot(newPivotPos);
    }

    public void RotateForward(bool reverse = false) 
      => rotate(_cameraDirections.CameraRight, reverse);

    public void RotateRight(bool reverse = false) 
      => rotate(_cameraDirections.CameraForward, reverse);

    private void rotate(Vector3Int axis, bool reverse)
    {
      if (IsNull(_currentShape))
        return;

      float angle = reverse ? -90f : 90f;

      Vector3Int originalEuler = _currentShape.CurrentRotation;
      Quaternion targetRotation = Quaternion.AngleAxis(angle, axis);
      Quaternion currRotation = Quaternion.Euler(originalEuler);

      Vector3Int newEuler = Vector3Int.RoundToInt(
        (targetRotation * currRotation).eulerAngles);

      _currentShape.SetRotation(newEuler);

      if (_map.AnySpaceOccupied(_currentShape.GridPositions))
        _currentShape.SetRotation(originalEuler);
    }

    private bool canMoveInDirection(Vector3Int dir)
    {
      foreach (var gridPosition in _currentShape.GridPositions)
      {
        Vector3Int checkedPos = gridPosition + dir;

        if (_map.PositionInGrid(checkedPos.x, checkedPos.z) == false)
          return false;

        if (_map.SpaceOccupied(checkedPos))
          return false;
      }

      return true;
    }
  }
}