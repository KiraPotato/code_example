﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using System.Collections;
using UnityEngine;

using Zenject;

using Tetris3D.Model;
using Tetris3D.Settings;

namespace Tetris3D.Controller
{
  public class PlayerInputController : MonoBehaviour
  {
    [Inject] private IPlayerInput _input = default;
    [Inject] private ShapeController _shapeMover = default;
    [Inject] private ShapeDropController _shapeDropper = default;
    [Inject] private GameSettings _gameSettings = default;
    [Inject] private CameraDirection _cameraDirections = default;

    bool _canDropShape = true;

    private void Update()
    {
      moveToSides();
      shapeDrop();
      shapeRotate();
    }

    private void shapeRotate()
    {
      if (_input.RotateForward)
        _shapeMover.RotateForward();
      else if (_input.RotateBack)
        _shapeMover.RotateForward(true);
      else if (_input.RotateLeft)
        _shapeMover.RotateRight();
      else if (_input.RotateRight)
        _shapeMover.RotateRight(true);
    }

    private void shapeDrop()
    {
      if (_canDropShape && _input.DropShape)
      {
        _shapeDropper.ForceShapeDrop();
        StartCoroutine(blockShapeDrop());
      }
    }

    private IEnumerator blockShapeDrop()
    {
      _canDropShape = false;
      yield return new WaitForSeconds(_gameSettings.ShapeMoveDownCooldown);
      _canDropShape = true;
    }

    private void moveToSides()
    {
      Vector3 inputDir = Vector3.zero;

      if (_input.MoveShapeForward)
        inputDir += _cameraDirections.CameraForward;

      if (_input.MoveShapeBack)
        inputDir -= _cameraDirections.CameraForward;

      if (_input.MoveShapeRight)
        inputDir += _cameraDirections.CameraRight;

      if (_input.MoveShapeLeft)
        inputDir -= _cameraDirections.CameraRight;

      _shapeMover.MoveShape(Mathf.RoundToInt(inputDir.x), Mathf.RoundToInt(inputDir.z));
    }
  }
}