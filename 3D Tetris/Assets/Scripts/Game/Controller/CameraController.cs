﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using UnityEngine;

using Sirenix.OdinInspector;
using Zenject;

using Tetris3D.Model;
using Tetris3D.Settings;
using Tetris3D.Utilities;

namespace Tetris3D.Controller
{
  public class CameraController : SerializedMonoBehaviour
  {
    public Camera Camera => _camera;

    [SerializeField, Required, ChildGameObjectsOnly]
    private Transform _rotationTransform = default;

    [SerializeField, Required, ChildGameObjectsOnly]
    private Transform _offsetTransform = default;

    [SerializeField, Required, ChildGameObjectsOnly]
    private Camera _camera = default;

    [Inject] private IPlayerInput _input = default;
    [Inject] private GameSettings _gameSettings = default;
    [Inject] private CameraDirection _cameraDirection = default;

    private void Awake()
    {
      if (NullChecker.IsNull(_rotationTransform, _offsetTransform, _camera))
        Debug.LogError(Constants.ERROR_MISSING_EDITOR_REFERENCES, this);
    }

    private void Start()
    {
      _rotationTransform.position = _gameSettings.CameraStartPosition;
      _offsetTransform.localPosition = _gameSettings.CameraOffsetPosition;
    }

    private void Update()
    {
      updateCameraPosition();
      _cameraDirection.UpdateCameraDirections(_rotationTransform.forward, _rotationTransform.right);
    }

    private void updateCameraPosition()
    {
      float cameraRotationValue = _input.CameraRotationValue;
      float deltaTime = Time.deltaTime;

      rotateCamera(cameraRotationValue, deltaTime);

      cameraLookAt();
    }

    private void rotateCamera(float rotValue, float deltaTime)
    {
      if (rotValue == 0f)
        return;

      Vector3 rotation = Vector3.up * rotValue * _gameSettings.CameraRotationSpeed * deltaTime;
      _rotationTransform.Rotate(rotation);
    }

    private void cameraLookAt()
    {
      _camera.transform.LookAt(_gameSettings.CameraLookAtPosition);
      Debug.DrawLine(_camera.transform.position, _gameSettings.CameraLookAtPosition);
    }

  }
}
