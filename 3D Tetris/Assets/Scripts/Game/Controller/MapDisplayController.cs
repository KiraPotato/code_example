﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using UnityEngine;

using Zenject;

using Tetris3D.Model;
using Tetris3D.Settings;
using Tetris3D.View;
using static Tetris3D.Utilities.NullChecker;

namespace Tetris3D.Controller
{
  public class MapDisplayController : MonoBehaviour
  {
    [Inject] private ShapeDrawer _shapeDrawer = default;
    [Inject] private MapData _map = default;
    [Inject] private ShapeQueue _shapeQueue = default;
    [Inject] private GameSettings _gameSettings = default;

    private IShapeData _currentShape => _shapeQueue.CurrentShape;

    private void LateUpdate()
    {
        drawCurrentShape();
        drawPlacedBlocks();
        drawBounderies();
        drawLandingPreview();
    }

    private void drawLandingPreview()
    {
      if (IsNull(_currentShape))
        return;

      foreach (var position in _currentShape.PreviewGridPositions)
      {
        _shapeDrawer.DrawBlock(position, _currentShape.PreviewMaterial);
      }
    }

    private void drawBounderies()
    {
      _shapeDrawer.DrawFrontBackWalls(_gameSettings.MapSize);
      _shapeDrawer.DrawSideWalls(_gameSettings.MapSize);
      _shapeDrawer.DrawFloor(_gameSettings.MapSize);
    }

    private void drawCurrentShape()
    {
      if (IsNull(_currentShape))
        return;

      foreach (var block in _currentShape.Blocks)
      {
        _shapeDrawer.DrawBlock(block.GridPosition, block.Material);
      }
    }

    private void drawPlacedBlocks()
    {
      foreach (var block in _map.PlacedBlocks)
      {
        _shapeDrawer.DrawBlock(block.GridPosition, block.Material);
      }
    }
  }
}