/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using UnityEngine;

using Zenject;
using Sirenix.OdinInspector;

using Tetris3D.Model;
using Tetris3D.Controller;
using Tetris3D.View;
using Tetris3D.Settings;
using Tetris3D.Events;
using Tetris3D.Managers;
using static Tetris3D.Utilities.NullChecker;

namespace Tetris3D.Installers
{
  public class GameSceneInstaller : MonoInstaller
  {
    [SerializeField, Required]
    private ShapeCollection _shapeCollection = default;

    [SerializeField, Required]
    private Camera _sceneCamera = default;

    [Inject]
    private GameSettings _gameSettings = default;

    [Inject]
    private IPlayerInput _playerInput = default;

    public override void InstallBindings()
    {
      if (IsNull(_shapeCollection, _sceneCamera, _gameSettings, _playerInput))
        Debug.LogError(Constants.ERROR_MISSING_EDITOR_REFERENCES, this);

      Container.
        Bind<IPlayerInput>().
        FromInstance(_playerInput).
        AsSingle().
        NonLazy();

      createNewGoAndComponent<ShapeDrawer>();
      createNewGoAndComponent<ShapeDropController>();
      createNewGoAndComponent<SceneGameManager>();
      createNewGoAndComponent<PlayerInputController>();
      createNewGoAndComponent<MapDisplayController>();
      createNewGoAndComponent<ShapeController>();

      bindSimpleClass<LocalGameState>();
      bindSimpleClass<ShapeQueue>();
      bindSimpleClass<CameraDirection>();
      bindSimpleClass<ShapeGenerator>();

      bindSimpleClass<OnShapeCollidedEvent>();
      bindSimpleClass<OnGameOverEvent>();
      bindSimpleClass<OnScoreChangedEvent>();

      bindFromScene<CameraController>();

      bindToInstance(_shapeCollection);
      bindToInstance(_sceneCamera);
      bindToInstance(new MapData(_gameSettings.MapSize));
    }

    private void bindFromScene<T>() where T : Component
    {
      Container.
        BindInterfacesAndSelfTo<T>().
        FromComponentInHierarchy().
        AsSingle().
        NonLazy();
    }

    private void bindToInstance<T>(T instance)
    {
      Container.
        BindInterfacesAndSelfTo<T>().
        FromInstance(instance).
        AsSingle().
        NonLazy();
    }

    private void bindSimpleClass<T>()
    {
      Container.
        BindInterfacesAndSelfTo<T>().
        AsSingle().
        NonLazy();
    }

    private void createNewGoAndComponent<T>() where T : Component
    {
      Container.
        BindInterfacesAndSelfTo<T>().
        FromNewComponentOnNewGameObject().
        AsSingle().
        NonLazy();
    }
  }
}