/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using Sirenix.OdinInspector;

using UnityEngine;
using Zenject;

using Tetris3D.Settings;
using Tetris3D.Model;
using static Tetris3D.Utilities.NullChecker;

namespace Tetris3D.Installers
{
  public class GameInstaller : MonoInstaller
  {
    [SerializeField, Required]
    private PlayerInputSettings _defaultInput = default;

    [SerializeField, Required]
    private GameSettingsSo _gameSettings = default;

    [SerializeField, Required]
    private DisplaySettings _displaySettings = default;

    public override void InstallBindings()
    {
      if (IsNull(_defaultInput, _gameSettings, _displaySettings))
        Debug.LogError(Constants.ERROR_MISSING_EDITOR_REFERENCES, this);

      Container.
        Bind<IPlayerInput>().
        FromInstance(_defaultInput).
        AsSingle().
        NonLazy();

      Container.
        BindInterfacesAndSelfTo<GameSettingsSo>().
        FromInstance(_gameSettings).
        AsSingle().
        NonLazy();

      Container.
        BindInterfacesAndSelfTo<DisplaySettings>().
        FromInstance(_displaySettings).
        AsSingle()
        .NonLazy();

      Container.
        BindInterfacesAndSelfTo<LeaderboardData>().
        AsSingle()
        .NonLazy();
      Container.
        BindInterfacesAndSelfTo<GameSettings>().
        AsSingle().
        NonLazy();
    }
  }
}