﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using UnityEngine;
using TMPro;

namespace Tetris3D.View
{
  [RequireComponent(typeof(TextMeshProUGUI))]
  public class ScoreTextDispaly : MonoBehaviour
  {
    private TextMeshProUGUI _text;

    private void Awake()
    {
      _text = GetComponent<TextMeshProUGUI>();
    }

    public void UpdateScore(int score)
      => _text.text = score.ToString();
  }
}