﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

using Sirenix.OdinInspector;

using static Tetris3D.Utilities.NullChecker;

namespace Tetris3D.View
{
  public class MultiplayerGameOverScreen : SerializedMonoBehaviour
  {
    public UnityEvent OnExitButtonClicked { get; } = new OnExitButtonClickedEvent();

    [SerializeField, Required]
    private Button _exitButton = default;

    [SerializeField, Required]
    private TextMeshProUGUI _winText = default;

    [SerializeField, Required]
    private TextMeshProUGUI _player1Score = default;

    [SerializeField, Required]
    private TextMeshProUGUI _player2Score = default;

    private void Awake()
    {
      if (IsNull(_exitButton, _winText, _player1Score, _player2Score))
        Debug.LogError(Constants.ERROR_MISSING_EDITOR_REFERENCES, this);
    }

    public void EnableScreen(Player whoLost, int player1Score, int player2Score)
    {
      _exitButton.onClick.AddListener(OnExitButtonClicked.Invoke);

      string playerName = string.Empty;

      switch (whoLost)
      {
        case Player.Player1:
          playerName = Constants.PLAYER_NAME_2;
          break;
        case Player.Player2:
          playerName = Constants.PLAYER_NAME_1;
          break;
      }

      _winText.text = string.Format(_winText.text, playerName);
      _player1Score.text = player1Score.ToString();
      _player2Score.text = player2Score.ToString();

      gameObject.SetActive(true);
    }

    private class OnExitButtonClickedEvent : UnityEvent { }
  }
}