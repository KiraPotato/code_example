﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using TMPro;
using UnityEngine;

using Sirenix.OdinInspector;

using static Tetris3D.Utilities.NullChecker;

namespace Tetris3D.View
{
  public class LeaderboardEntryDisplay : SerializedMonoBehaviour
  {
    [SerializeField, Required, ChildGameObjectsOnly]
    private TextMeshProUGUI _score = default;

    [SerializeField, Required, ChildGameObjectsOnly]
    private TextMeshProUGUI _name = default;

    private void Awake()
    {
      if (IsNull(_score, _name))
        Debug.LogError(Constants.ERROR_MISSING_EDITOR_REFERENCES, this);
    }

    public void SetValues(string name, int score)
    {
      _name.text = name;
      _score.text = score.ToString();
    }
  }
}