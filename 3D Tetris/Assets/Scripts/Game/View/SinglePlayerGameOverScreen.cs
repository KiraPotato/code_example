﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

using Sirenix.OdinInspector;

using static Tetris3D.Utilities.NullChecker;

namespace Tetris3D.View
{
  public class SinglePlayerGameOverScreen : MonoBehaviour
  {
    public UnityEvent OnExitButtonClicked { get; } = new OnExitButtonClickedEvent();
    public string CurrentInputName => _nameInput.text;

    [SerializeField, Required]
    private TextMeshProUGUI _scoreText = default;

    [SerializeField, Required]
    private Button _exitButton = default;

    [SerializeField, Required]
    private TMP_InputField _nameInput = default;

    private void Awake()
    {
      if (IsNull(_scoreText, _exitButton, _nameInput))
        Debug.LogError(Constants.ERROR_MISSING_EDITOR_REFERENCES, this);
    }

    public void EnableScreen(int score)
    {
      _exitButton.onClick.AddListener(OnExitButtonClicked.Invoke);

      _scoreText.text = score.ToString();
      gameObject.SetActive(true);
    }

    private class OnExitButtonClickedEvent : UnityEvent { }
  }
}