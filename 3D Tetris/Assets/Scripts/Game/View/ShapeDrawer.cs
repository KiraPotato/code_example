﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using System.Collections.Generic;
using UnityEngine;

using Zenject;

using Tetris3D.Settings;

namespace Tetris3D.View
{
  public class ShapeDrawer : MonoBehaviour
  {
    private static Vector3 _borderPanelSize = Vector3.one;
    private static Vector3 _forwardWallRotation = new Vector3(0f, 0f, 0f);
    private static Vector3 _backWallRotation = new Vector3(180f, 0f, 0f);
    private static Vector3 _rightWallRotation = new Vector3(0f, 90f, 0f);
    private static Vector3 _leftWallRotation = new Vector3(0f, -90f, 0f);
    private static Vector3 _floorRotation = new Vector3(90f, 0f, 0f);

    [Inject] private Camera _localCamera = default;

    private const float OFFSET = -.5f;

    [Inject] private DisplaySettings _displaySettings = default;

    public void DrawFloor(Vector3Int mapSize)
    {
      for (int x = 0; x < mapSize.x; x++)
      {
        for (int z = 0; z < mapSize.z; z++)
        {
          Vector3 position = new Vector3(x, OFFSET, z);
          DrawPlane(position, _borderPanelSize, _floorRotation, _displaySettings.BorderMaterial);
        }
      }
    }

    public void DrawFrontBackWalls(Vector3Int mapSize)
    {
      for (int x = 0; x < mapSize.x; x++)
      {
        for (int y = 0; y < mapSize.y; y++)
        {
          Vector3 forwardPosition = new Vector3(x, y, mapSize.z + OFFSET);
          Vector3 backwardPosition = new Vector3(x, y, OFFSET);

          DrawPlane(forwardPosition, _borderPanelSize, _forwardWallRotation, _displaySettings.BorderMaterial);
          DrawPlane(backwardPosition, _borderPanelSize, _backWallRotation, _displaySettings.BorderMaterial);

        }
      }
    }

    public void DrawSideWalls(Vector3Int mapSize)
    {
      for (int z = 0; z < mapSize.z; z++)
      {
        for (int y = 0; y < mapSize.y; y++)
        {
          Vector3 rightPosition = new Vector3(mapSize.x + OFFSET, y, z);
          Vector3 leftPosition = new Vector3(OFFSET, y, z);

          DrawPlane(rightPosition, _borderPanelSize, _rightWallRotation, _displaySettings.BorderMaterial);
          DrawPlane(leftPosition, _borderPanelSize, _leftWallRotation, _displaySettings.BorderMaterial);

        }
      }
    }

    public void DrawPlane(Vector3 worldPosition, Vector3 scale, Vector3 rotation, Material material)
    {
      Matrix4x4 matrix = Matrix4x4.TRS(worldPosition, Quaternion.Euler(rotation), scale);
      Graphics.DrawMesh(_displaySettings.PlaneMesh, matrix, material, 0, _localCamera);
    }

    public void DrawBlock(Vector3 worldPosition, Material material)
      => Graphics.DrawMesh(_displaySettings.CubeMesh, worldPosition, transform.rotation, material, 0, _localCamera);

    public void DrawBlocks(Material material, IReadOnlyCollection<Vector3Int> gridPositions)
    {
      foreach (var gridPosition in gridPositions)
        DrawBlock(gridPosition, material);
    }
  }
}