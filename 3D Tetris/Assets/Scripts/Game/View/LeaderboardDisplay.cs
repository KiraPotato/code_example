﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using Sirenix.OdinInspector;
using UnityEngine;
using Zenject;

using Tetris3D.Model;
using static Tetris3D.Utilities.NullChecker;

namespace Tetris3D.View
{
  public class LeaderboardDisplay : SerializedMonoBehaviour
  {
    [Inject] private LeaderboardData leaderboard = default;

    [SerializeField, Required, AssetsOnly]
    private LeaderboardEntryDisplay _entryPrefab = default;

    private void Awake()
    {
      if (IsNull(_entryPrefab))
        Debug.LogError(Constants.ERROR_MISSING_EDITOR_REFERENCES, this);
    }

    private void Start()
    {
      if (leaderboard.Entries.Count == 0)
      {
        Destroy(gameObject);
        return;
      }

      foreach (var item in leaderboard.Entries)
      {
        var entry = Instantiate(_entryPrefab, transform);
        entry.SetValues(item.Name, item.Score);
      }
    }
  }
}