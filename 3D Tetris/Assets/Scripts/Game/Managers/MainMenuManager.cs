﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

using Sirenix.OdinInspector;

using static Tetris3D.Utilities.NullChecker;

namespace Tetris3D.Managers
{
  public class MainMenuManager : MonoBehaviour
  {
    [SerializeField, Required]
    private Button _singlePlayerButton = default;

    [SerializeField, Required]
    private Button _coopButton = default;

    [SerializeField, Required]
    private Button _exitButton = default;

    private void Awake()
    {
      if (IsNull(_singlePlayerButton, _coopButton, _exitButton))
        Debug.LogError(Constants.ERROR_MISSING_EDITOR_REFERENCES, this);

      _singlePlayerButton.onClick.AddListener(
        () => SceneManager.LoadScene(Constants.SCENE_NAME_SINGLEPLAYER));

      _coopButton.onClick.AddListener(
        () => SceneManager.LoadScene(Constants.SCENE_NAME_COOP));

      _exitButton.onClick.AddListener(
        Application.Quit);
    }
  }
}