﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using Sirenix.OdinInspector;
using Zenject;

using Tetris3D.Controller;
using Tetris3D.Model;
using Tetris3D.Utilities;
using Tetris3D.View;
using static Tetris3D.Utilities.NullChecker;
using Tetris3D.Events;

namespace Tetris3D.Managers
{
  public class MultiplayerManager : SerializedMonoBehaviour
  {
    private static Rect _leftPlayerRect = new Rect(0f, 0f, .5f, 1f);
    private static Rect _righPlayerRect = new Rect(.5f, 0f, .5f, 1f);

    [SerializeField, Required]
    private Camera _camera = default;

    [SerializeField, Required]
    public MultiplayerGameOverScreen _gameOverScreen = default;

    [SerializeField, Required]
    private Dictionary<Player, PlayerReferences> _playerReferences =
      new Dictionary<Player, PlayerReferences>();

    private DiContainer _projectContext;
    private LoadSceneParameters _sceneParams;

    private void Awake()
    {
      if (IsNull(_camera, _gameOverScreen, _playerReferences) || _playerReferences.Count == 0)
        Debug.LogError(Constants.ERROR_MISSING_EDITOR_REFERENCES, this);

      _sceneParams = new LoadSceneParameters(LoadSceneMode.Additive);
      _projectContext = FindObjectOfType<ProjectContext>().Container;

      _gameOverScreen.OnExitButtonClicked.AddListener(
        () => SceneManager.LoadScene(Constants.SCENE_NAME_MAIN_MENU));
    }

    private void Start()
    {
      StartCoroutine(loadGame());
    }

    private IEnumerator loadGame()
    {
      yield return loadScene(Player.Player1);
      yield return loadScene(Player.Player2);

      setCameraRects(_leftPlayerRect, Player.Player1);
      setCameraRects(_righPlayerRect, Player.Player2);

      _camera.enabled = false;
    }

    private void setCameraRects(Rect rect, Player targetPlayer)
      => _playerReferences[targetPlayer].SceneContext.Resolve<CameraController>().Camera.rect = rect;

    private void bindPlayerReferneces(Player player)
    {
      var gameState = _playerReferences[player].SceneContext.Resolve<LocalGameState>();

      _playerReferences[player].SceneContext.Resolve<OnGameOverEvent>().AddListener(() => onPlayerLost(player));
      _playerReferences[player].SceneContext.Resolve<OnScoreChangedEvent>().AddListener(() => onScoreChanged(player, gameState.Score));

      _playerReferences[player].ScoreDisplay.UpdateScore(gameState.Score);
      _playerReferences[player].GameState = gameState;
    }

    private void onScoreChanged(Player player, int score)
    {
      _playerReferences[player].ScoreDisplay.UpdateScore(score);
    }

    private void onPlayerLost(Player player)
    {
      foreach (var item in _playerReferences)
      {
        item.Value.ScoreDisplay.gameObject.SetActive(false);
      }

      _camera.enabled = true;

      int score1 = _playerReferences[Player.Player1].GameState.Score;
      int score2 = _playerReferences[Player.Player2].GameState.Score;

      _gameOverScreen.EnableScreen(player, score1, score2);
    }

    private IEnumerator loadScene(Player player)
    {
      yield return null;

      _projectContext.Rebind<IPlayerInput>().FromInstance(_playerReferences[player].Input).AsCached().NonLazy();

      var scene = SceneManager.LoadScene(Constants.SCENE_NAME_GAME, _sceneParams);

      yield return new WaitUntil(() => scene.isLoaded);

      _playerReferences[player].Scene = scene;
      _playerReferences[player].SceneContext = 
        SceneManagementUtilities.FetchSceneContext(scene);

      bindPlayerReferneces(player);
    }

    public class PlayerReferences
    {
      public Scene Scene { get; set; }
      public LocalGameState GameState { get; set; }
      public DiContainer SceneContext { get; set; }

      public IPlayerInput Input => _input;
      public ScoreTextDispaly ScoreDisplay => _scoreDisplay;

      [SerializeField, Required]
      private IPlayerInput _input = default;

      [SerializeField, Required]
      private ScoreTextDispaly _scoreDisplay = default;
    }
  }
}