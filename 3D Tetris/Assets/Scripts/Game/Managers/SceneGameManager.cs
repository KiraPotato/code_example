﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using System.Collections.Generic;
using UnityEngine;

using Zenject;
using Sirenix.OdinInspector;

using Tetris3D.Events;
using Tetris3D.Model;
using Tetris3D.Controller;

namespace Tetris3D.Managers
{
  public class SceneGameManager : MonoBehaviour
  {
    [Inject] private MapData _map = default;
    [Inject] private ShapeQueue _shapeQueue = default;
    [Inject] private OnShapeCollidedEvent _onShapeCollided = default;
    [Inject] private LocalGameState _gameState = default;
    [Inject] private OnGameOverEvent _onGameOverEvent = default;

    private IShapeData _currentShape => _shapeQueue.CurrentShape;
    private List<int> _tempYStorage;
    private bool _gameEnded;

    private void Awake()
    {
      _tempYStorage = new List<int>();
      _gameEnded = false;
      _onShapeCollided.AddListener(onShapeCollided);
    }

    private void Start()
    {
      _shapeQueue.SelectNext();
    }

#if UNITY_EDITOR
    [Button]
    private void testAddScore(int score = 500)
      => _gameState.AddScore(score);

    [Button]
    private void testGameOver(int gameOver)
      => _onGameOverEvent.Invoke();
#endif

    private void onShapeCollided()
    {
      if (_gameEnded)
        return;

      _tempYStorage.Clear();

      foreach (IBlock block in _currentShape.Blocks)
      {
        if (_tempYStorage.Contains(block.GridPosition.y) == false)
          _tempYStorage.Add(block.GridPosition.y);

        _map.AddToMap(block);
      }

      if (_tempYStorage.Contains(_map.MapSize.y))
      {
        _onGameOverEvent.Invoke();
        _gameEnded = true;
        return;
      }

      int removedLayers = 0;

      foreach (var y in _tempYStorage)
      {
        if (_map.LayerFilled(y))
        {
          removedLayers++;
          _map.RemoveFromMap(y);
        }
      }

      if (removedLayers > 0)
      {
        _gameState.AddScore(100 * removedLayers);
      }

      _map.PlacedShapesCount++;
      _shapeQueue.ClearCurrentShape();
      _shapeQueue.SelectNext();
    }
  }
}