﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

using Sirenix.OdinInspector;
using Zenject;

using Tetris3D.View;
using Tetris3D.Model;
using Tetris3D.Utilities;
using static Tetris3D.Utilities.NullChecker;
using Tetris3D.Events;

namespace Tetris3D.Managers
{
  public class SingleplayerManager : SerializedMonoBehaviour
  {
    [SerializeField, Required]
    private Camera _camera = default;

    [SerializeField, Required]
    private IPlayerInput _input = default;

    [SerializeField, Required]
    private ScoreTextDispaly _scoreTextDisplay = default;

    [SerializeField, Required]
    private SinglePlayerGameOverScreen _gameOverScreen = default;

    [Inject]
    private LeaderboardData _leaderboard = default;

    private DiContainer _projectContext;
    private LoadSceneParameters _sceneParams;
    private Scene _gameScene;
    private LocalGameState _gameState;

    private void Awake()
    {
      if (IsNull(_camera, _input, _scoreTextDisplay, _gameOverScreen))
        Debug.LogError(Constants.ERROR_MISSING_EDITOR_REFERENCES, this);

      _sceneParams = new LoadSceneParameters(LoadSceneMode.Additive);
      _projectContext = FindObjectOfType<ProjectContext>().Container;
      _gameOverScreen.OnExitButtonClicked.AddListener(onExitClicked);
    }

    private void Start()
    {
      StartCoroutine(loadGameScene(onGameSceneLoaded));
    }

    private void onExitClicked()
    {
      if (string.IsNullOrEmpty(_gameOverScreen.CurrentInputName) == false)
        _leaderboard.AddEntry(_gameOverScreen.CurrentInputName, _gameState.Score);

      SceneManager.LoadScene(Constants.SCENE_NAME_MAIN_MENU);
    }

    private void onGameSceneLoaded(DiContainer sceneContext)
    {
      LocalGameState gameState = sceneContext.Resolve<LocalGameState>();

      sceneContext.Resolve<OnGameOverEvent>().AddListener(() => onGameOver(gameState));
      sceneContext.Resolve<OnScoreChangedEvent>().AddListener(() => onScoreChanged(gameState));

      _gameState = gameState;

      _scoreTextDisplay.UpdateScore(gameState.Score);
    }

    private void onScoreChanged(LocalGameState gameState)
    {
      _scoreTextDisplay.UpdateScore(gameState.Score);
    }

    private void onGameOver(LocalGameState gameState)
    {
      _scoreTextDisplay.gameObject.SetActive(false);
      _gameOverScreen.EnableScreen(gameState.Score);
      _camera.enabled = true;
      SceneManager.UnloadSceneAsync(_gameScene);
    }

    private IEnumerator loadGameScene(System.Action<DiContainer> onSceneLoaded)
    {
      _projectContext.Rebind<IPlayerInput>().FromInstance(_input).AsCached().NonLazy();

      _gameScene = SceneManager.LoadScene(Constants.SCENE_NAME_GAME, _sceneParams);

      yield return new WaitUntil(() => _gameScene.isLoaded);

      _camera.enabled = false;

      onSceneLoaded?.Invoke(SceneManagementUtilities.FetchSceneContext(_gameScene));
    }

  }
}