﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using UnityEngine;

namespace Tetris3D
{
  public interface IBlock
  {
    Vector3Int GridPosition { get; set; }
    Material Material { get; }
  }
}