﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using System.Collections.Generic;
using UnityEngine;

namespace Tetris3D
{
  public interface IShapeData
  {
    Vector3Int CurrentRotation { get; }
    Vector3Int PivotPosition { get; }
    Material NormalMaterial { get; }
    Material PreviewMaterial { get; }
    IReadOnlyCollection<IBlock> Blocks { get; }
    IReadOnlyCollection<Vector3Int> GridPositions { get; }
    IReadOnlyCollection<Vector3Int> Offsets { get; }
    IReadOnlyCollection<Vector3Int> PreviewGridPositions { get; }
    Vector3Int PreviewOffset { get; set; }

    void MovePivot(Vector3Int newPivotPosition);
    void SetRotation(Vector3Int rotation);
  }
}