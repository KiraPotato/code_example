﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
namespace Tetris3D
{
  public interface IPlayerInput
  {
    bool MoveShapeForward { get; }
    bool MoveShapeBack { get; }
    bool MoveShapeRight { get; }
    bool MoveShapeLeft { get; }
    bool DropShape { get; }

    bool RotateLeft { get; }
    bool RotateRight { get; }
    bool RotateForward { get; }
    bool RotateBack { get; }

    float CameraRotationValue { get; }
  }
}
