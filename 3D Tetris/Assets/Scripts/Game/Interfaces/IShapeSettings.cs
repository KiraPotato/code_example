﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using UnityEngine;

namespace Tetris3D
{
  public interface IShapeSettings
  {
    Color ShapeBaseColor { get; }
    float Weight { get; }

    IShapeData GenerateShape(Vector3Int pivotStartPosition, Material shapeMaterial, Material previewMaterial);
  }
}