﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
namespace Tetris3D
{
  public static class Constants
  {
    public const string ERROR_MISSING_EDITOR_REFERENCES = "Some references were not setup properly in the editor!";

    public const string LEADERBOARD_PREF_NAME = "LEADERBOARD_JSON";
    public const int MAX_LEADERBOARD_COUNT = 7;

    public const string PLAYER_NAME_1 = "Player 1";
    public const string PLAYER_NAME_2 = "Player 2";

    public const string SCENE_NAME_MAIN_MENU = "Menu";
    public const string SCENE_NAME_GAME = "GameScene";
    public const string SCENE_NAME_SINGLEPLAYER = "Singleplayer";
    public const string SCENE_NAME_COOP = "Multiplayer";

    public const string ROOT_NODE = "Settings";
    public const string NODE_GAME_SETTINGS = "GameSettings";
    public const string NODE_CUSTOM_SHAPES = "CustomShapes";

    public const string ATTRIBUTE_OVERRIDE = "overrideValue";
    public const string ATTRIBUTE_VALUE = "value";

    public const string ATTRIBUTE_VECTOR_X = "x";
    public const string ATTRIBUTE_VECTOR_Y = "y";
    public const string ATTRIBUTE_VECTOR_Z = "z";

    public const string ATTRIBUTE_COLOR_R = "r";
    public const string ATTRIBUTE_COLOR_G = "g";
    public const string ATTRIBUTE_COLOR_B = "b";
    public const string ATTRIBUTE_COLOR_A = "a";

    public const string NODE_CAMERA_LOOK_AT_POSITION = "CameraLookAtPosition";
    public const string NODE_CAMERA_START_POSITION = "CameraStartPosition";
    public const string NODE_CAMERA_OFFSET_POSITION = "CameraOffsetPosition";
    public const string NODE_CAMERA_ROTATION_SPEED = "CameraRotationSpeed";
    public const string NODE_SHAPE_SPAWN_POSITION = "ShapeSpawnPosition";
    public const string NODE_MAP_SIZE = "MapSize";
    public const string NODE_SHAPE_MOVE_COOLDOWN = "ShapeMoveDownCooldown";
    public const string NODE_DROP_COOLDOWN_DECREASE_PER_SHAPE = "DropCooldownDecreasePerShape";
    public const string NODE_DROP_COOLDOWN = "DropCooldown";
    public const string NODE_SHAPE = "Shape";
    public const string NODE_COLOR = "Color";
    public const string NODE_PICK_WEIGHT = "PickWeight";
    public const string NODE_OFFSETS = "Offsets";
  }
}
