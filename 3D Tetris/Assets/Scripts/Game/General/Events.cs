﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using UnityEngine.Events;

namespace Tetris3D.Events
{
  public class OnShapeCollidedEvent : UnityEvent { }
  public class OnGameOverEvent : UnityEvent { }
  public class OnScoreChangedEvent :UnityEvent { }
}