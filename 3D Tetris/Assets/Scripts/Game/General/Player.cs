﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
namespace Tetris3D
{
  public enum Player
  {
    Player1,
    Player2
  }
}
