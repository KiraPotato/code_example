﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

using UnityEngine;

using static Tetris3D.Constants;
using Tetris3D.Utilities;

namespace Tetris3D.Settings
{
  public class GameSettingsXml
  {
    public LoadedSetting<Vector3> CameraLookAtPosition => _cameraLookAtPosition;
    public LoadedSetting<Vector3> CameraStartPosition => _cameraStartPosition;
    public LoadedSetting<Vector3> CameraOffsetPosition => _cameraOffsetPosition;
    public LoadedSetting<float> CameraRotationSpeed => _cameraRotationSpeed;
    public LoadedSetting<Vector3Int> ShapeSpawnPosition => _shapeSpawnPosition;
    public LoadedSetting<Vector3Int> MapSize => _mapSize;
    public LoadedSetting<float> ShapeMoveDownCooldown => _shapeMoveDownCooldown;
    public LoadedSetting<float> DropCooldownDecreasePerShape => _dropCooldownDecreasePerShape;
    public LoadedSetting<Vector2> DropCooldown => _dropCooldown;
    public IReadOnlyCollection<IShapeSettings> CustomShapes => _customShapes;

    private List<IShapeSettings> _customShapes = new List<IShapeSettings>();
    private LoadedSetting<Vector3> _cameraLookAtPosition = new LoadedSetting<Vector3>();
    private LoadedSetting<Vector3> _cameraStartPosition = new LoadedSetting<Vector3>();
    private LoadedSetting<Vector3> _cameraOffsetPosition = new LoadedSetting<Vector3>();
    private LoadedSetting<float> _cameraRotationSpeed = new LoadedSetting<float>();
    private LoadedSetting<Vector3Int> _shapeSpawnPosition = new LoadedSetting<Vector3Int>();
    private LoadedSetting<Vector3Int> _mapSize = new LoadedSetting<Vector3Int>();
    private LoadedSetting<float> _shapeMoveDownCooldown = new LoadedSetting<float>();
    private LoadedSetting<float> _dropCooldownDecreasePerShape = new LoadedSetting<float>();
    private LoadedSetting<Vector2> _dropCooldown = new LoadedSetting<Vector2>();

    public static GameSettingsXml Translate(string xmlData)
    {

      if (string.IsNullOrEmpty(xmlData))
        return null;

      var settings = new GameSettingsXml();

      XmlDocument doc = new XmlDocument();
      doc.Load(new StringReader(xmlData));

      XmlNode rootNode = doc.SelectSingleNode(ROOT_NODE);

      if (rootNode == null)
        return null;

      loadGameSettings(rootNode.SelectSingleNode(NODE_GAME_SETTINGS), ref settings);
      loadCustomShapes(rootNode.SelectSingleNode(NODE_CUSTOM_SHAPES), ref settings);

      return settings;
    }

    private static void loadGameSettings(XmlNode mainNode, ref GameSettingsXml savedData)
    {
      if (mainNode == null)
        return;

      saveVector3(mainNode.SelectSingleNode(NODE_CAMERA_LOOK_AT_POSITION), ref savedData._cameraLookAtPosition);
      saveVector3(mainNode.SelectSingleNode(NODE_CAMERA_START_POSITION), ref savedData._cameraStartPosition);
      saveVector3(mainNode.SelectSingleNode(NODE_CAMERA_OFFSET_POSITION), ref savedData._cameraOffsetPosition);
      saveFloat(mainNode.SelectSingleNode(NODE_CAMERA_ROTATION_SPEED), ref savedData._cameraRotationSpeed);

      saveVector3Int(mainNode.SelectSingleNode(NODE_SHAPE_SPAWN_POSITION), ref savedData._shapeSpawnPosition);
      saveVector3Int(mainNode.SelectSingleNode(NODE_MAP_SIZE), ref savedData._mapSize);

      saveFloat(mainNode.SelectSingleNode(NODE_SHAPE_MOVE_COOLDOWN), ref savedData._shapeMoveDownCooldown);
      saveFloat(mainNode.SelectSingleNode(NODE_DROP_COOLDOWN_DECREASE_PER_SHAPE), ref savedData._dropCooldownDecreasePerShape);
      saveVector2(mainNode.SelectSingleNode(NODE_DROP_COOLDOWN), ref savedData._dropCooldown);
    }

    #region Custom Shapes
    private static void loadCustomShapes(XmlNode shapesNode, ref GameSettingsXml savedData)
    {
      if (shapesNode == null)
        return;

      try
      {
        foreach (XmlNode node in shapesNode.SelectNodes(NODE_SHAPE))
        {
          LoadedShapeSettings shape = loadShape(node);

          if (shape != null)
            savedData._customShapes.Add(shape);
        }
      }
      catch (Exception) { }
    }

    private static LoadedShapeSettings loadShape(XmlNode shapeNode)
    {
      if (shapeNode == null)
        return null;

      Color color = XmlUtilities.GetColor(shapeNode.SelectSingleNode(NODE_COLOR));
      float weight = XmlUtilities.GetFloat(shapeNode.SelectSingleNode(NODE_PICK_WEIGHT), ATTRIBUTE_VALUE);

      List<Vector3Int> offsets = new List<Vector3Int>();

      try
      {
        foreach (XmlNode node in shapeNode.SelectNodes(NODE_OFFSETS))
        {
          offsets.Add(XmlUtilities.GetVector3Int(node));
        }

      }
      catch (Exception) { }

      return new LoadedShapeSettings(color, weight, offsets);
    }
    #endregion

    #region Save
    private static void saveFloat(XmlNode node, ref LoadedSetting<float> saveTo)
    {
      try
      {
        if (saveTo == null)
          saveTo = new LoadedSetting<float>();

        saveTo.Save(getOverrideBool(node), XmlUtilities.GetFloat(node, ATTRIBUTE_VALUE));
      }
      catch (Exception) { }
    }

    private static void saveVector3Int(XmlNode node, ref LoadedSetting<Vector3Int> saveTo)
    {
      try
      {
        if (saveTo == null)
          saveTo = new LoadedSetting<Vector3Int>();

        saveTo.Save(getOverrideBool(node), XmlUtilities.GetVector3Int(node));
      }
      catch (Exception) { }
    }

    private static void saveVector3(XmlNode node, ref LoadedSetting<Vector3> saveTo)
    {
      try
      {
        if (saveTo == null)
          saveTo = new LoadedSetting<Vector3>();

        saveTo.Save(getOverrideBool(node), XmlUtilities.GetVector3(node));
      }
      catch (Exception) { }
    }

    private static void saveVector2(XmlNode node, ref LoadedSetting<Vector2> saveTo)
    {
      try
      {
        if (saveTo == null)
          saveTo = new LoadedSetting<Vector2>();

        saveTo.Save(getOverrideBool(node), XmlUtilities.GetVector2(node));
      }
      catch (Exception) { }
    }

    private static bool getOverrideBool(XmlNode node) => XmlUtilities.GetBool(node, ATTRIBUTE_OVERRIDE);
    #endregion

    public class LoadedSetting<ValueType>
    {
      public bool Override;
      public ValueType Value;

      public LoadedSetting()
      {
        Override = false;
      }

      public void Save(bool overrideValue, ValueType value)
      {
        Override = overrideValue;
        Value = value;
      }

      public ValueType GetValue(ValueType defaultValue)
      {
        if (Override == false)
          return defaultValue;

        return Value;
      }
    }
  }
}