﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using UnityEngine;
using UnityEngine.UIElements;

using Sirenix.OdinInspector;

namespace Tetris3D.Settings
{
  [CreateAssetMenu]
  public class GameSettingsSo : SerializedScriptableObject
  {
    private const float MIN_DROP_COOLDOWN = .05f;
    private const float MAX_DROP_COOLDOWN = 10f;

    public int MaxQueueSize => _queueSize.y;
    public int MinQueueSize => _queueSize.x;
    public Vector3Int MapSize => _mapSize;

    public Vector3Int ShapeSpawnPosition => _shapeSpawnPosition;
    public float ShapeMoveDownCooldown => _shapeMoveDownCooldown;

    public Vector3 CameraStartPosition => _cameraStartPosition;
    public Vector3 CameraOffsetPosition => _cameraOffsetPosition;
    public float CameraRotationSpeed => _cameraRotationSpeed;
    public Vector3 CameraLookAtPosition => _cameraLookAtPosition;
    public Vector2 DropCooldown => _dropCooldown;
    public float DropCooldownDecreasePerShape => _dropCooldownDecreasePerShape;
    public string XmlContent
    {
      get
      {
        if (_xmlFile == null)
          return string.Empty;

        return _xmlFile.text;
      }
    }

    [SerializeField, FoldoutGroup("Camera")]
    private Vector3 _cameraLookAtPosition = default;

    [SerializeField, FoldoutGroup("Camera")]
    private Vector3 _cameraStartPosition = default;

    [SerializeField, FoldoutGroup("Camera")]
    private Vector3 _cameraOffsetPosition = default;

    [SerializeField, FoldoutGroup("Camera")]
    private float _cameraRotationSpeed = 10f;

    [SerializeField, MinMaxSlider(1, 100, ShowFields = true)]
    private Vector2Int _queueSize = Vector2Int.one;

    [SerializeField, FoldoutGroup("Map")]
    private Vector3Int _shapeSpawnPosition = default;

    [SerializeField, FoldoutGroup("Map")]
    private Vector3Int _mapSize = default;

    [SerializeField, FoldoutGroup("Gameplay")]
    private float _shapeMoveDownCooldown = .2f;

    [SerializeField, FoldoutGroup("Gameplay"), MinMaxSlider(MIN_DROP_COOLDOWN, MAX_DROP_COOLDOWN, ShowFields = true)]
    private Vector2 _dropCooldown = Vector2.up;

    [SerializeField, FoldoutGroup("Gameplay")]
    private float _dropCooldownDecreasePerShape = 0.01f;

    [SerializeField, FoldoutGroup("Custom Settings")]
    private TextAsset _xmlFile = default;
  }
}