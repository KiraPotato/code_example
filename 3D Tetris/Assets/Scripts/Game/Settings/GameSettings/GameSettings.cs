﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using System.Collections.Generic;
using UnityEngine;

namespace Tetris3D.Settings
{
  public class GameSettings
  {
    public IReadOnlyCollection<IShapeSettings> CustomShapes => _customShapes;
    public int MaxQueueSize { get; private set; }
    public int MinQueueSize { get; private set; }
    public Vector3Int MapSize { get; private set; }
    public Vector3Int SpawnPosition
    {
      get
      {
        if (_spawnPositionGenerated == false)
        {
          _spawnPosition = generateBlockSpawnPosition();
          _spawnPositionGenerated = true;
        }

        return _spawnPosition;
      }
    }
    public Vector3Int ShapeSpawnPosition { get; private set; }
    public float ShapeMoveDownCooldown { get; private set; }
    public Vector3 CameraStartPosition { get; private set; }
    public Vector3 CameraOffsetPosition { get; private set; }
    public float CameraRotationSpeed { get; private set; }
    public Vector3 CameraLookAtPosition { get; private set; }

    private Vector3Int _spawnPosition;
    private bool _spawnPositionGenerated;

    private float _dropCooldownDecreasePerShape;
    private Vector2 _dropCooldown;
    private List<IShapeSettings> _customShapes;

    public GameSettings(GameSettingsSo settings)
    {
      MaxQueueSize = settings.MaxQueueSize;
      MinQueueSize = settings.MinQueueSize;
      MapSize = settings.MapSize;
      ShapeSpawnPosition = settings.ShapeSpawnPosition;
      ShapeMoveDownCooldown = settings.ShapeMoveDownCooldown;

      CameraStartPosition = settings.CameraStartPosition;
      CameraOffsetPosition = settings.CameraOffsetPosition;
      CameraLookAtPosition = settings.CameraLookAtPosition;
      CameraRotationSpeed = settings.CameraRotationSpeed;

      _dropCooldown = settings.DropCooldown;
      _dropCooldownDecreasePerShape = settings.DropCooldownDecreasePerShape;

      var xml = GameSettingsXml.Translate(settings.XmlContent);
      if (xml == null)
        return;

      CameraLookAtPosition = xml.CameraLookAtPosition.GetValue(CameraLookAtPosition);
      CameraStartPosition = xml.CameraStartPosition.GetValue(CameraStartPosition);
      CameraOffsetPosition = xml.CameraOffsetPosition.GetValue(CameraOffsetPosition);
      CameraRotationSpeed = xml.CameraRotationSpeed.GetValue(CameraRotationSpeed);

      MapSize = xml.MapSize.GetValue(MapSize);
      ShapeMoveDownCooldown = xml.ShapeMoveDownCooldown.GetValue(ShapeMoveDownCooldown);
      _dropCooldownDecreasePerShape = xml.DropCooldownDecreasePerShape.GetValue(_dropCooldownDecreasePerShape);
      _dropCooldown = xml.DropCooldown.GetValue(_dropCooldown);

      _customShapes = new List<IShapeSettings>(xml.CustomShapes);
    }

    public float FetchShapeDropCooldown(int placedShapesCount)
    {
      float cooldown = _dropCooldown.y - placedShapesCount * _dropCooldownDecreasePerShape;
      return Mathf.Clamp(cooldown, _dropCooldown.x, _dropCooldown.y);
    }

    private Vector3Int generateBlockSpawnPosition()
    {
      Vector3Int spawnPosition = Vector3Int.zero;

      spawnPosition.y = MapSize.y;
      spawnPosition.x = MapSize.x / 2;
      spawnPosition.z = MapSize.z / 2;

      return spawnPosition;
    }
  }
}