﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using UnityEngine;

using Sirenix.OdinInspector;
using System.Collections.Generic;

namespace Tetris3D.Settings
{
  [CreateAssetMenu]
  public class DisplaySettings : SerializedScriptableObject
  {
    private const string LINE_COLOR = "_LineColor";
    private const string FILL_COLOR = "_FillColor";

    public Mesh CubeMesh => _cubeMesh;
    public Mesh PlaneMesh => _planeMesh;
    public Material BorderMaterial => _borderMaterial;
    public Material PreviewMaterial => _previewMaterial;
    public Color FillShadeColor => _fillShadeColor;

    [SerializeField, Required]
    private Material _borderMaterial = default;

    [SerializeField, Required]
    private Material _baseShapeMaterial = default;

    [SerializeField, Required]
    private Material _previewMaterial = default;

    [SerializeField, Required]
    private Mesh _cubeMesh = default;

    [SerializeField, Required]
    private Mesh _planeMesh = default;

    [SerializeField, Required]
    private Color _fillShadeColor = default;

    private Dictionary<Color, Material> _materialMapping = new Dictionary<Color, Material>();

    public Material FetchMaterial(Color mainColor)
    {
      if (_materialMapping == null)
        _materialMapping = new Dictionary<Color, Material>();

      if (_materialMapping.ContainsKey(mainColor) == false)
      {
        Material mat = new Material(_baseShapeMaterial);
        mat.SetColor(LINE_COLOR, mainColor);
        mat.SetColor(FILL_COLOR, mainColor * _fillShadeColor);
        _materialMapping[mainColor] = mat;
      }

      return _materialMapping[mainColor];
    }
  }
}