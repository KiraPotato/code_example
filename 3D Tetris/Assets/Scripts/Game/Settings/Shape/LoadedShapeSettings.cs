﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using System.Collections.Generic;
using UnityEngine;

using Tetris3D.Model;

namespace Tetris3D.Settings
{
  [System.Serializable]
  public class LoadedShapeSettings : IShapeSettings
  {
    public Color ShapeBaseColor => _shapeBaseColor;
    public float Weight => _pickWeight;

    [SerializeField]
    private float _pickWeight = 10f;

    [SerializeField]
    private Color _shapeBaseColor = default;

    [SerializeField]
    private List<Vector3Int> _offsets = new List<Vector3Int>();

    public LoadedShapeSettings(Color color, float weight, IReadOnlyCollection<Vector3Int> offsets)
    {
      _offsets = new List<Vector3Int>(offsets);
      _shapeBaseColor = color;
      _pickWeight = weight;
    }

    public IShapeData GenerateShape(Vector3Int pivotStartPosition, Material shapeMaterial, Material previewMaterial)
      => new ShapeData(pivotStartPosition, _offsets, shapeMaterial, previewMaterial);
  }
}