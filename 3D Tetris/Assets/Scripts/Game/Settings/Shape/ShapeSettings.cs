﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using System.Collections.Generic;
using UnityEngine;

using Sirenix.OdinInspector;

using Tetris3D.Model;

namespace Tetris3D.Settings
{
  [CreateAssetMenu]
  public class ShapeSettings : SerializedScriptableObject, IShapeSettings
  {
    public float Weight => _pickWeight;
    public Color ShapeBaseColor => _shapeBaseColor;

    [SerializeField]
    List<Vector3Int> _offsets = new List<Vector3Int>();

    [SerializeField, Range(0f, 100f)]
    private float _pickWeight = 10f;

    [SerializeField, Required]
    private Color _shapeBaseColor = default;

    public IShapeData GenerateShape(Vector3Int pivotStartPosition, Material shapeMaterial, Material previewMaterial)
      => new ShapeData(pivotStartPosition, _offsets, shapeMaterial, previewMaterial);
  }
}