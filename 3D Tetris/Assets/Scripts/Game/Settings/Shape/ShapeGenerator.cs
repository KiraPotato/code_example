﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using UnityEngine;

using Sirenix.OdinInspector;
using System.Collections.Generic;
using Tetris3D.Utilities;

namespace Tetris3D.Settings
{
  public class ShapeGenerator
  {
    private static Shapes _shapes;
    private ShapeCollection _shapeCollection;
    private DisplaySettings _displaySettings;
    private GameSettings _gameSettings;

    public ShapeGenerator(DisplaySettings displaySettings, ShapeCollection shapeCollection, GameSettings gameSettings)
    {
      _shapeCollection = shapeCollection;
      _displaySettings = displaySettings;
      _gameSettings = gameSettings;
    }

    public IShapeData GenerateRandomShape(Vector3Int startPosition)
    {
      if (_shapes == null)
        _shapes = new Shapes(_shapeCollection.AvailableShapes, _gameSettings.CustomShapes);

      IShapeSettings settings = _shapes.FetchRandomShape();
      Material material = _displaySettings.FetchMaterial(settings.ShapeBaseColor);

      return settings.GenerateShape(startPosition, material, _displaySettings.PreviewMaterial);
    }

    private class Shapes
    {
      [SerializeField, ReadOnly]
      private float _maxWeights;

      [SerializeField, ReadOnly]
      private Dictionary<float, IShapeSettings> _shapes;

      private float _offset;

      public Shapes(IReadOnlyCollection<IShapeSettings> shapes, IReadOnlyCollection<IShapeSettings> customShapes)
      {
        _shapes = new Dictionary<float, IShapeSettings>();
        _offset = 0f;

        AddShapes(shapes);
        AddShapes(customShapes);
      }

      public void AddShapes(IReadOnlyCollection<IShapeSettings> shapes)
      {
        if (NullChecker.IsNull(shapes))
          return;

        foreach (var shape in shapes)
          addShape(shape);

        _maxWeights = _offset;
      }

      private void addShape(IShapeSettings shape)
      {
        float currWeight = shape.Weight;
        _shapes[_offset] = shape;

        _offset += currWeight;
      }

      public IShapeSettings FetchRandomShape()
      {
        float x = Random.Range(0f, _maxWeights);

        foreach (var entry in _shapes)
        {
          IShapeSettings settings = entry.Value;
          float offset = entry.Key + settings.Weight;

          if (offset >= x)
            return settings;
        }

        return default;
      }
    }
  }
}