﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using System.Collections.Generic;

using UnityEngine;

using Sirenix.OdinInspector;

namespace Tetris3D.Settings
{
  [CreateAssetMenu]
  public class ShapeCollection : SerializedScriptableObject
  {
    public IReadOnlyCollection<IShapeSettings> AvailableShapes => _availableShapes;

    [SerializeField, Required]
    private List<IShapeSettings> _availableShapes = new List<IShapeSettings>();
  }
}