﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using UnityEngine;

using Sirenix.OdinInspector;

namespace Tetris3D.Settings
{
  [CreateAssetMenu]
  public class PlayerInputSettings : SerializedScriptableObject, IPlayerInput
  {
    private const string INPUT_SHAPE_MOVE_LABEL = "Shape Movement";
    private const string INPUT_SHAPE_ROTATION_LABEL = "Shape Rotation";
    private const string INPUT_CAMERA_ROTATION_LABEL = "Camera Rotation";

    public float CameraRotationValue
    {
      get
      {
        if (Input.GetKey(_rotateCameraLeft))
          return -1f;
        else if (Input.GetKey(_rotateCameraRight))
          return 1f;

        return 0f;
      }
    }

    public bool MoveShapeForward => Input.GetKeyDown(_forward);
    public bool MoveShapeBack => Input.GetKeyDown(_back);
    public bool MoveShapeRight => Input.GetKeyDown(_right);
    public bool MoveShapeLeft => Input.GetKeyDown(_left);

    public bool RotateLeft => Input.GetKeyDown(_leftRotate);
    public bool RotateRight => Input.GetKeyDown(_rightRotate);
    public bool RotateForward => Input.GetKeyDown(_forwardRotate);
    public bool RotateBack => Input.GetKeyDown(_backRotate);

    public bool DropShape => Input.GetKey(_dropShape);

    [SerializeField, FoldoutGroup(INPUT_SHAPE_MOVE_LABEL)]
    private KeyCode _left = KeyCode.A;

    [SerializeField, FoldoutGroup(INPUT_SHAPE_MOVE_LABEL)]
    private KeyCode _right = KeyCode.D;

    [SerializeField, FoldoutGroup(INPUT_SHAPE_MOVE_LABEL)]
    private KeyCode _forward = KeyCode.W;

    [SerializeField, FoldoutGroup(INPUT_SHAPE_MOVE_LABEL)]
    private KeyCode _back = KeyCode.S;

    [SerializeField, FoldoutGroup(INPUT_SHAPE_MOVE_LABEL)]
    private KeyCode _dropShape = KeyCode.Space;

    [SerializeField, FoldoutGroup(INPUT_SHAPE_ROTATION_LABEL)]
    private KeyCode _leftRotate = KeyCode.J;

    [SerializeField, FoldoutGroup(INPUT_SHAPE_ROTATION_LABEL)]
    private KeyCode _rightRotate = KeyCode.L;

    [SerializeField, FoldoutGroup(INPUT_SHAPE_ROTATION_LABEL)]
    private KeyCode _forwardRotate = KeyCode.I;

    [SerializeField, FoldoutGroup(INPUT_SHAPE_ROTATION_LABEL)]
    private KeyCode _backRotate = KeyCode.K;

    [SerializeField, FoldoutGroup(INPUT_CAMERA_ROTATION_LABEL)]
    private KeyCode _rotateCameraLeft = KeyCode.Q;

    [SerializeField, FoldoutGroup(INPUT_CAMERA_ROTATION_LABEL)]
    private KeyCode _rotateCameraRight = KeyCode.E;

  }
}