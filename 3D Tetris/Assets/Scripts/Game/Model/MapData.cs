﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using System.Collections.Generic;
using UnityEngine;

using Sirenix.OdinInspector;

using static Tetris3D.Utilities.NullChecker;

namespace Tetris3D.Model
{
  public class MapData
  {
    public Vector3Int MapSize { get; private set; }
    public IReadOnlyCollection<IBlock> PlacedBlocks => _map.Values;
    public IReadOnlyCollection<Vector3Int> OccupiedCells => _map.Keys;
    public int PlacedShapesCount
    {
      get => _placedShapes;
      set => _placedShapes = value;
    }

    [SerializeField, ReadOnly]
    private int _placedShapes = 0;

    [SerializeField, ReadOnly]
    private Dictionary<Vector3Int, IBlock> _map;

    public MapData(Vector3Int mapSize)
    {
      MapSize = mapSize;
      _map = new Dictionary<Vector3Int, IBlock>();
    }

    #region Checks
    public bool AnySpaceOccupied(IReadOnlyCollection<Vector3Int> gridPositions)
    {
      foreach (var position in gridPositions)
      {
        if (SpaceOccupied(position))
          return true;

        if (PositionInGrid(position.x, position.z) == false)
          return true;

        if (position.y < 0)
          return true;
      }

      return false;
    }

    public bool SpaceOccupied(Vector3Int gridPosition)
      => _map.ContainsKey(gridPosition);

    public bool LayerFilled(int y)
    {
      for (int x = 0; x < MapSize.x; x++)
      {
        for (int z = 0; z < MapSize.z; z++)
        {
          Vector3Int currPos = new Vector3Int(x, y, z);

          if (SpaceOccupied(currPos) == false)
            return false;
        }
      }

      return true;
    }

    public bool PositionInGrid(int x, int z)
    {
      if (x < 0 || z < 0)
        return false;

      if (x >= MapSize.x || z >= MapSize.z)
        return false;

      return true;
    }

    public bool PositionInGrid(Vector3Int checkedPosition)
    {
      if (checkedPosition.x < 0
          || checkedPosition.y < 0
          || checkedPosition.z < 0)
        return false;

      if (checkedPosition.x >= MapSize.x
          || checkedPosition.y >= MapSize.y
          || checkedPosition.z >= MapSize.z)
        return false;

      return true;
    }
    #endregion

    #region Add
    public void AddToMap(IReadOnlyCollection<IBlock> placedBlocks)
    {
      if (IsNull(placedBlocks))
        return;

      foreach (var block in placedBlocks)
        AddToMap(block);
    }

    public void AddToMap(IBlock block)
    {
      if (block == null)
        return;

      if (SpaceOccupied(block.GridPosition))
        return;

      _map[block.GridPosition] = block;
    }
    #endregion

    #region Remove
    public void RemoveFromMap(int layer)
    {
      for (int x = 0; x < MapSize.x; x++)
      {
        for (int z = 0; z < MapSize.z; z++)
        {
          Vector3Int currPos = new Vector3Int(x, layer, z);
          RemoveFromMap(currPos);

          for (int y = layer; y < MapSize.y; y++)
          {
            currPos.y = y;

            moveBlock(currPos, currPos + Vector3Int.down);
          }
        }
      }

      if (LayerFilled(layer))
        RemoveFromMap(layer);
    }

    private void moveBlock(Vector3Int blockPosition, Vector3Int targetPosition)
    {
      if (SpaceOccupied(blockPosition) == false)
        return;

      if (SpaceOccupied(targetPosition))
        return;

      IBlock block = _map[blockPosition];
      block.GridPosition = targetPosition;

      _map.Remove(blockPosition);
      _map[targetPosition] = block;
    }

    public void RemoveFromMap(Block block)
    {
      if (block == null)
        return;

      RemoveFromMap(block.GridPosition);
    }

    public void RemoveFromMap(Vector3Int gridPosition)
    {
      if (SpaceOccupied(gridPosition) == false)
        return;

      _map.Remove(gridPosition);
    }
    #endregion
  }  
}