﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using System.Collections.Generic;
using UnityEngine;

using Sirenix.OdinInspector;

using Tetris3D.Settings;

namespace Tetris3D.Controller
{
  public class ShapeQueue
  {
    public IShapeData CurrentShape => _currentShape;
    public IReadOnlyCollection<IShapeData> CurrentQueue => _shapeQueue;

    private Vector3Int _shapeSpawnPosition;
    private ShapeGenerator _shapeGenerator;
    private GameSettings _gameSettings;

    [SerializeField, ReadOnly]
    private Queue<IShapeData> _shapeQueue;

    [SerializeField, ReadOnly]
    private IShapeData _currentShape;

    public ShapeQueue(GameSettings gameSettings, ShapeGenerator shapeGenerator)
    {
      _shapeGenerator = shapeGenerator;
      _gameSettings = gameSettings;
      _shapeSpawnPosition = gameSettings.ShapeSpawnPosition;

      _shapeQueue = new Queue<IShapeData>();
      _currentShape = null;
    }

    public void SelectNext()
    {
      fillQueue();

      _currentShape = _shapeQueue.Dequeue();
      CurrentShape.MovePivot(_shapeSpawnPosition);
    }

    public void ClearCurrentShape() => _currentShape = null;

    private void fillQueue()
    {
      Vector3Int shapeQueuePos = Vector3Int.zero;

      while (_shapeQueue.Count < _gameSettings.MaxQueueSize)
      {
        _shapeQueue.Enqueue(_shapeGenerator.GenerateRandomShape(shapeQueuePos));
      }
    }
  }
}