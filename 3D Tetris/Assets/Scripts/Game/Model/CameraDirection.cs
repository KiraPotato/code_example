﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using UnityEngine;

namespace Tetris3D.Model
{
  public class CameraDirection
  {
    public Vector3Int CameraForward { get; private set; }
    public Vector3Int CameraRight { get; private set; }

    public void UpdateCameraDirections(Vector3 cameraForward, Vector3 cameraRight)
    {
      if (Mathf.Abs(cameraRight.z) > Mathf.Abs(cameraRight.x))
        cameraRight.x = 0f;
      else
        cameraRight.z = 0f;

      if (Mathf.Abs(cameraForward.z) > Mathf.Abs(cameraForward.x))
        cameraForward.x = 0f;
      else
        cameraForward.z = 0f;

      CameraForward = Vector3Int.RoundToInt(cameraForward);
      CameraRight = Vector3Int.RoundToInt(cameraRight);
    }
  }
}