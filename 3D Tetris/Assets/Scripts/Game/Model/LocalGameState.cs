﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using Tetris3D.Events;

namespace Tetris3D.Model
{
  public class LocalGameState
  {
    public int Score { get; private set; }

    private OnScoreChangedEvent _onScoreChanged;

    public LocalGameState(OnScoreChangedEvent onScoreChanged)
    {
      _onScoreChanged = onScoreChanged;
      Score = 0;
    }

    public void AddScore(int addedScore)
    {
      Score += addedScore;
      _onScoreChanged.Invoke();
    }
  }
}