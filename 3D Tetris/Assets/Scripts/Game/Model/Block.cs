﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using UnityEngine;

using Sirenix.OdinInspector;

namespace Tetris3D.Model
{

  public class Block : IBlock
  {
    public Vector3Int GridPosition
    {
      get => _gridPosition;
      set => _gridPosition = value;
    }

    public Material Material => _material;

    [SerializeField, ReadOnly]
    private Vector3Int _gridPosition;

    [SerializeField, ReadOnly]
    private Material _material;

    public Block(Vector3Int gridPosition, Material material)
    {
      _gridPosition = gridPosition;
      _material = material;
    }
  }
}