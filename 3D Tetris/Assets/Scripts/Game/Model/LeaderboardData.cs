﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Tetris3D.Model
{
  public class LeaderboardData : IDisposable
  {

    public IReadOnlyCollection<Leaderboard.LeaderboardEntry> Entries => _leaderboard.Entries;

    private Leaderboard _leaderboard;

    public LeaderboardData()
    {
      _leaderboard = fetchLeaderboard();
    }

    public void AddEntry(string name, int score)
    {
      _leaderboard.AddEntry(name, score);
      save();
    }

    public void Dispose()
    {
      save();
    }

    private void save()
    {
      PlayerPrefs.SetString(Constants.LEADERBOARD_PREF_NAME, JsonUtility.ToJson(_leaderboard));
    }

    private Leaderboard fetchLeaderboard()
    {
      if (PlayerPrefs.HasKey(Constants.LEADERBOARD_PREF_NAME))
      {
        try
        {
          string leaderboardSave = PlayerPrefs.GetString(Constants.LEADERBOARD_PREF_NAME);
          return JsonUtility.FromJson<Leaderboard>(leaderboardSave);
        }
        catch (Exception) { }
      }

      return new Leaderboard();
    }


    [System.Serializable]
    public class Leaderboard
    {
      public IReadOnlyCollection<LeaderboardEntry> Entries => _entries;

      [SerializeField]
      private List<LeaderboardEntry> _entries = new List<LeaderboardEntry>();

      public void AddEntry(string name, int score)
      {
        var entry = new LeaderboardEntry(name, score);
        _entries.Add(entry);
        _entries.Sort();

        while (_entries.Count > Constants.MAX_LEADERBOARD_COUNT)
          _entries.Remove(_entries.Last());
      }

      [System.Serializable]
      public struct LeaderboardEntry : IComparable<LeaderboardEntry>
      {
        public string Name => _name;
        public int Score => _score;

        [SerializeField]
        private string _name;

        [SerializeField]
        private int _score;

        public LeaderboardEntry(string name, int score)
        {
          _score = score;
          _name = name;
        }

        public int CompareTo(LeaderboardEntry other) => other.Score.CompareTo(Score);
      }
    }
  }
}