﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using System.Collections.Generic;
using UnityEngine;

using static Tetris3D.Utilities.VectorUtilities;

namespace Tetris3D.Model
{
  public class ShapeData : IShapeData
  {
    private const string EXCEPTION_NULL_OR_BLANK_OFFSETS = "Recieved offsets list was either null or empty!";

    public Material NormalMaterial { get; private set; }
    public Material PreviewMaterial { get; private set; }
    public Vector3Int CurrentRotation => _rotation;
    public Vector3Int PivotPosition => _pivotPosition;
    public IReadOnlyCollection<IBlock> Blocks => _shapeParts.Values;
    public IReadOnlyCollection<Vector3Int> Offsets => _shapeParts.Keys;
    public IReadOnlyCollection<Vector3Int> GridPositions => _gridPositions;
    public IReadOnlyCollection<Vector3Int> PreviewGridPositions
    {
      get
      {
        if (_previewDirty)
          recalculatePreviewOffsetList();

        return _previewGridPositions;
      }
    }
    public Vector3Int PreviewOffset
    {
      get => _previewOffset;
      set
      {
        if (_previewOffset != value)
          _previewDirty = true;

        _previewOffset = value;
      }
    }

    private Vector3Int _rotation;
    private Vector3Int _pivotPosition;

    private Dictionary<Vector3Int, Block> _shapeParts;
    private List<Vector3Int> _gridPositions;
    private List<Vector3Int> _previewGridPositions;
    private Vector3Int _previewOffset;

    private bool _previewDirty;

    public ShapeData(
      Vector3Int pivotStartPosition,
      IReadOnlyCollection<Vector3Int> offsets,
      Material normalMaterial, Material previewMaterial)
    {
      if (offsets == null || offsets.Count == 0)
        throw new System.NullReferenceException(EXCEPTION_NULL_OR_BLANK_OFFSETS);

      NormalMaterial = normalMaterial;
      PreviewMaterial = previewMaterial;

      _previewGridPositions = new List<Vector3Int>();
      _shapeParts = new Dictionary<Vector3Int, Block>();
      _gridPositions = new List<Vector3Int>();
      _previewDirty = true;
      _pivotPosition = pivotStartPosition;

      foreach (var offset in offsets)
      {
        Vector3Int gridPosition = pivotStartPosition + offset;
        _shapeParts[offset] = new Block(gridPosition, normalMaterial);
        _gridPositions.Add(gridPosition);
      }
    }

    public void MovePivot(Vector3Int newPivotPosition)
    {
      _pivotPosition = newPivotPosition;

      _previewDirty = true;
      updateBlockGrids();
    }

    public void SetRotation(Vector3Int rotation)
    {
      _rotation = rotation;
      _previewDirty = true;
      updateBlockGrids();
    }

    private void updateBlockGrids()
    {
      _gridPositions.Clear();

      foreach (var entry in _shapeParts)
      {
        Vector3Int offset = entry.Key;

        Block block = entry.Value;
        Vector3Int newGridPosition = _pivotPosition + RotateAround(offset, Vector3Int.zero, _rotation);

        block.GridPosition = newGridPosition;
        _gridPositions.Add(newGridPosition);
      }
    }

    private void recalculatePreviewOffsetList()
    {
      _previewGridPositions.Clear();

      foreach (var gridPosition in GridPositions)
      {
        _previewGridPositions.Add(gridPosition - _previewOffset);
      }
    }
  }
}
