﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using UnityEngine;

namespace Tetris3D.Utilities
{
  public static class VectorUtilities
  {
    public static Vector3Int RotateAround(Vector3Int point, Vector3Int pivot, Vector3Int angle)
      => Vector3Int.RoundToInt(RotateAround(point, pivot, (Vector3)angle));

    public static Vector3 RotateAround(Vector3 point, Vector3 pivot, Vector3 angle)
    {
      Vector3 dir = point - pivot;
      dir = Quaternion.Euler(angle) * dir;
      point = dir + pivot;
      return point;
    }
  }
}