﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
namespace Tetris3D.Utilities
{
  public static class NullChecker
  {
    public static bool IsNull(params object[] objects)
    {
      if (objects.Length == 0)
        return true;
      else if (objects.Length == 1)
        return IsObjectNull(objects[0]);
      else
      {
        foreach (var item in objects)
        {
          if (IsObjectNull(item))
            return true;
        }

        return false;
      }
    }

    public static bool NotNull(params object[] objects)
    => IsNull(objects) == false;

    private static bool IsObjectNull(object targetObject)
      => targetObject == null || targetObject.Equals(null);
  }
}