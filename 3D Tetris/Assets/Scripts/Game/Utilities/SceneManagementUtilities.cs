﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using UnityEngine.SceneManagement;

using Zenject;

namespace Tetris3D.Utilities
{
  public static class SceneManagementUtilities
  {
    public static DiContainer FetchSceneContext(Scene scene)
    {
      var sceneObjects = scene.GetRootGameObjects();
      SceneContext sceneContext = null;

      for (int i = 0; i < sceneObjects.Length; i++)
      {
        sceneContext = sceneObjects[i].GetComponent<SceneContext>();

        if (sceneContext != null)
          break;
      }

      return sceneContext.Container;
    }
  }
}