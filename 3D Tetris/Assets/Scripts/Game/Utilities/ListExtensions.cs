﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using System.Collections.Generic;
using UnityEngine;

namespace Tetris3D.Utilities
{
  public static class ListExtensions
  {
    public static T FetchRandom<T>(this List<T> list)
    {
      if (list.Count == 0)
        return default;

      return list[Random.Range(0, list.Count)];
    }
  }
}