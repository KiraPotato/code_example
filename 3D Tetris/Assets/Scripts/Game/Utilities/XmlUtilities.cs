﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using System;
using System.Xml;
using UnityEngine;

using static Tetris3D.Constants;

namespace Tetris3D.Utilities
{
  public static class XmlUtilities
  {
    public static Color GetColor(XmlNode node)
    {
      try
      {
        var r = GetFloat(node, ATTRIBUTE_COLOR_R);
        var g = GetFloat(node, ATTRIBUTE_COLOR_G);
        var b = GetFloat(node, ATTRIBUTE_COLOR_B);
        var a = GetFloat(node, ATTRIBUTE_COLOR_A);

        return new Color(r, g, b, a);
      }
      catch (Exception)
      {
        return Color.white;
      }
    }

    public static Vector3Int GetVector3Int(XmlNode node)
    {
      try
      {
        var x = GetInt(node, ATTRIBUTE_VECTOR_X);
        var y = GetInt(node, ATTRIBUTE_VECTOR_Y);
        var z = GetInt(node, ATTRIBUTE_VECTOR_Z);

        return new Vector3Int(x, y, z);
      }
      catch (Exception)
      {
        return default;
      }
    }

    public static Vector3 GetVector3(XmlNode node)
    {
      try
      {
        var x = GetFloat(node, ATTRIBUTE_VECTOR_X);
        var y = GetFloat(node, ATTRIBUTE_VECTOR_Y);
        var z = GetFloat(node, ATTRIBUTE_VECTOR_Z);

        return new Vector3(x, y, z);
      }
      catch (Exception)
      {
        return default;
      }
    }

    public static Vector2 GetVector2(XmlNode node)
    {
      try
      {
        var x = GetFloat(node, ATTRIBUTE_VECTOR_X);
        var y = GetFloat(node, ATTRIBUTE_VECTOR_Y);

        return new Vector2(x, y);
      }
      catch (Exception)
      {
        return default;
      }
    }

    public static int GetInt(XmlNode node, string attributeName)
    {
      try
      {
        return int.Parse(node.Attributes[attributeName].Value);
      }
      catch (Exception)
      {
        return default;
      }
    }

    public static float GetFloat(XmlNode node, string attributeName)
    {
      try
      {
        return float.Parse(node.Attributes[attributeName].Value);
      }
      catch (Exception)
      {
        return default;
      }
    }

    public static bool GetBool(XmlNode node, string attributeName)
    {
      try
      {
        return Convert.ToBoolean(node.Attributes[attributeName].Value);
      }
      catch (System.Exception)
      {
        return default;
      }
    }
  }
}