﻿/*
 * Author: Kirill Gnevishev
 * Date: November 2020
 * Email: kirazpotato@gmail.com
 */
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;

using Tetris3D;
using Tetris3D.Model;

namespace Tests
{
  public class Shape_Data_Tests
  {
    private const string SHADER_NAME = "Shader Graphs/BlockShader";
    private const string LINE_COLOR = "_LineColor";
    private const string FILL_COLOR = "_FillColor";

    private IShapeData _shape;
    private Vector3Int _pivot;
    private List<Vector3Int> _offsets;
    private Material _normMaterial;
    private Material _previewMaterial;
    private Vector3Int _rotation;

    [Test]
    public void TEST_00_SHAPE_CREATED()
    {
      _pivot = Vector3Int.one;
      _offsets = new List<Vector3Int>
      {
        Vector3Int.zero,
        Vector3Int.up
      };

      _previewMaterial = new Material(Shader.Find(SHADER_NAME));
      _previewMaterial.SetColor(LINE_COLOR, Color.white);
      _previewMaterial.SetColor(FILL_COLOR, Color.black);

      _normMaterial = new Material(_previewMaterial);

      _shape = new ShapeData(_pivot, _offsets, _normMaterial, _previewMaterial);

      Assert.NotNull(_shape, "Shape failed to create with constructor");
    }

    [Test]
    public void TEST_01_SHAPE_HAS_CORRECT_PIVOT_AFTER_CREATION()
    {
      Assert.AreEqual(_pivot, _shape.PivotPosition, "Pivot not set correctly in constructor");
    }

    [Test]
    public void TEST_02_CHECK_MATERIALS_SETUP()
    {
      Assert.AreEqual(_normMaterial, _shape.NormalMaterial, "Normal material was not saved correctly");
      Assert.AreEqual(_previewMaterial, _shape.PreviewMaterial, "Preview material was not saved correctly");
    }

    [Test]
    public void TEST_03_CHECK_ALL_LIST_COUNTS()
    {
      Assert.AreEqual(2, _shape.Offsets.Count, "Offset count was incorrect");
      Assert.AreEqual(2, _shape.Blocks.Count, "Blocks count was incorrect");
      Assert.AreEqual(2, _shape.GridPositions.Count, "Grid Positions count was incorrect");
    }

    [Test]
    public void TEST_04_CHECK_OFFSETS_CORRECT()
    {
      int i = 0;
      foreach (var shapeOffset in _shape.Offsets)
      {
        Assert.AreEqual(_offsets[i], shapeOffset, "One of the offsets was incorrect");

        i++;
      }
    }

    [Test]
    public void TEST_05_GRID_POSITIONS_CORRECT()
    {
      int i = 0;
      foreach (var shapeGridPosition in _shape.GridPositions)
      {
        Vector3Int targetPos = _offsets[i] + _pivot;

        Assert.AreEqual(targetPos, shapeGridPosition, "Grid position wasnt properly set based on pivot+offset");

        i++;
      }
    }

    [Test]
    public void TEST_06_BLOCKS_NOT_NULL()
    {
      foreach (var block in _shape.Blocks)
      {
        Assert.NotNull(block, "A created block was null");
      }
    }


    [Test]
    public void TEST_07_BLOCKS_HAS_CORRECT_MATERIAL()
    {
      foreach (var block in _shape.Blocks)
      {
        Assert.AreEqual(_normMaterial, block.Material, "Material wasnt set for block correctly on shape creation");
      }
    }

    [Test]
    public void TEST_08_BLOCKS_HAS_CORRECT_GRID_POSITION()
    {
      var i = 0;

      foreach (var block in _shape.Blocks)
      {
        Vector3Int targetPos = _offsets[i] + _pivot;

        Assert.AreEqual(targetPos, block.GridPosition, "Block grid positin wasnt set correctly on shape creation");

        i++;
      }
    }

    [Test]
    public void TEST_09_PIVOT_CHANGED_CORRECTLY()
    {
      _pivot = Vector3Int.zero;

      _shape.MovePivot(_pivot);

      Assert.AreEqual(_pivot, _shape.PivotPosition, "Pivot didnt move correctly using MovePivot method");
    }

    [Test]
    public void TEST_10_CHECK_OFFSETS_CORRECT_AFTER_PIVOT_MOVE()
    {
      int i = 0;
      foreach (var shapeOffset in _shape.Offsets)
      {
        Assert.AreEqual(_offsets[i], shapeOffset, "One of the offsets was incorrect after pivot moved");

        i++;
      }
    }

    [Test]
    public void TEST_11_BLOCKS_HAS_CORRECT_GRID_POSITION_AFTER_PIVOT_MOVE()
    {
      var i = 0;

      foreach (var block in _shape.Blocks)
      {
        Vector3Int targetPos = _offsets[i] + _pivot;

        Assert.AreEqual(targetPos, block.GridPosition, "Block grid positin wasnt set correctly after pivot moved");

        i++;
      }
    }

    [Test]
    public void TEST_12_ROTATION_WAS_APPLIED()
    {
      _rotation = new Vector3Int(180, 0, 0);
      _shape.SetRotation(_rotation);

      Assert.AreEqual(_rotation, _shape.CurrentRotation, "Rotation was not applied correctly using SetRotation method");
    }

    [Test]
    public void TEST_13_CHECK_OFFSETS_DIDNT_MOVE_AFTER_ROTATION()
    {
      int i = 0;
      foreach (var shapeOffset in _shape.Offsets)
      {
        Assert.AreEqual(_offsets[i], shapeOffset, "One of the offsets moved during rotation even though it shouldnt");

        i++;
      }
    }

    [Test]
    public void TEST_14_CHECK_NEW_GRID_POSITIONS_AFTER_ROTATION()
    {
      var gridPositions = new List<Vector3Int>(_shape.GridPositions);

      Assert.AreEqual(-_offsets[1], gridPositions[1], "Block with offset were not rotated correctly");
    }

    [Test]
    public void TEST_15_CHECK_NEW_BLOCK_POSITION_AFTER_ROTATION()
    {
      var blocks = new List<IBlock>(_shape.Blocks);

      Assert.AreEqual(-_offsets[1], blocks[1].GridPosition, "Block with offset were not rotated correctly");
    }
  }
}
